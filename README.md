#How to use BMV Application
*Created and managed by Harshit Seksaria.*

*Only for members of Bela Bai M. Morarka Vidyamandir, Located at Nawalgarh (Raj.)*

Here is a quick reference on the **working** and **how to's** of BMV Application.

#Privileges:
------
There are different privileges or powers given to different post, i.e., Student, Monitor, Teacher, and Principal. Which are as follow:-

###Student
- Can access **Holidays**.
- Can access **Homework** of his/her own class.
- Can participate in his/her own class **Discussion**.

###Monitor
- Can access **Holidays**.
- Can access **Homework** of his/her own class.
- Can participate in his/her own class **Discussion**.
- Can add **new Homework** of his/her own class.

###Teacher
- Can access **Holidays**.
- Can access **Homework** of all the classes.
- Can participate in **Discussion** of all the classes.
- Can add **new Homework** of any class.
- Can add **new Holidays**.
- Can edit any **student's or monitor's information**, i.e., name, class, and post.

###Principal
- Can access **Holidays**.
- Can access **Homework** of all the classes.
- Can participate in **Discussion** of all the classes.
- Can add **new Homework** of any class.
- Can add **new Holidays**.
- Can edit any **student's or monitor's information**, i.e., name, class, and post.
- Can edit any **teacher's information**, i.e., name, class, and post.


#Quick Tutorials
---
Here are some **how to's** in terms of BMV Applications.

####Topics covered here are as follow:
- How to create a new account? (For everyone)
- How to edit your own information? Like name, class, post. (For everyone)
- How to sign out of the app? (For everyone)
- How to add a new Homework? (For monitors, teachers, principal)
- How to add a new Holiday? (For teachers, principal)
- How to edit another user's information? Like name, class, and post. (For teacher, principal)


Let's start off!

##How to create a new account?
----
*Allowed to: Everyone.*

In order to create a new account on BMV application, follow these steps:

**Step 1:-** Tap on **"CREATE A NEW ACCOUNT"** button on the starting page.

**Step 2:-** Fill up the respective information, i.e., name, class, and post. Tap on **"SIGN-UP"** button.

*Now, a new account request will raise on teacher's and principal's app. They will check if entered information is correct, if so then they will accept your request. But if you've entered something wrong then they will reject your request and will leave a message indicating the reason for rejection.*

*Wait until your request gets accepted! After then head to step 3*

**Step 3:-** Enter a new email and password so that you can open your account in other phones too by using these email and password. *Side note: Your email must contain "@bmv.com", for example, "harshit@bmv.com". And your email must be unique.*

**Step 4:-** Once everything's done, Tap on **"SIGN-UP"**.

And there you go, new account created for you!


##How to edit your own information? Like name, class, and post.
---
*Allowed to: Everyone.*

In order to edit your information like, name, class, and post, try following these steps:

**Step 1:-** Open up your app (Your account should be created), Tap on **"Users"**.

**Step 2:-** Tap on three dots at top of the screen (better known as "Overflow menu"), Tap on **"Edit yourself"**.

**Step 3:-** Fill your respective information, and tap **"SEND"**.

*This will send an edit request on teacher's and principal's app. They will check if entered information is correct, if so then they will accept your request. Elsewise they will reject your request*.

If your request gets accepted then your information will be updated!


##How to sign out of the app?
---
*Allowed to: Everyone*

You can sign out of the app by tapping on **"Sign out"**, located in the three dots (better known as Overflow menu) of the starting page.

Now, you can sign in with another account.

##How to add new Homework?
---
*Allowed to: Monitor, Teacher, Principal*

Head to the Homework section, tap on **"+"** button located at the bottom of the screen.

Add information accordingly, tap on **"Add"** and there you go!

##How to add new Holiday?
---
*Allowed to: Teacher, Principal*

Head to Holiday section, tap on **"+"** button located in the bottom of the screen.

Add information accordingly, tap on **"Add"** and there you go!

##How to edit another user's information? Like name, class, and post.
---
*Allowed to: Teacher, Principal*

If you come to know that anyone is having wrong info then you can change that.

Head to Users section, Long press on the user you want to change.

Add information accordingly, tap on **"Change"** and there you go!